package no.andrebw.springsecuritydemo.repository;

import no.andrebw.springsecuritydemo.exceptions.UserNotFoundException;
import no.andrebw.springsecuritydemo.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByUsername(String username) throws UserNotFoundException;

}
