package no.andrebw.springsecuritydemo.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import no.andrebw.springsecuritydemo.exceptions.InvalidTokenException;
import no.andrebw.springsecuritydemo.exceptions.UserAlreadyExistsException;
import no.andrebw.springsecuritydemo.exceptions.UserNotFoundException;
import no.andrebw.springsecuritydemo.model.*;
import no.andrebw.springsecuritydemo.repository.UserRepository;
import no.andrebw.springsecuritydemo.security.JwtService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private static final Logger logger = LogManager.getLogger(AuthenticationService.class);

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationmanager;

    @Transactional
    public User register(UserDTO userDTO) {

        User user = User.builder()
                .username(userDTO.getUsername())
                .password(passwordEncoder.encode(userDTO.getPassword()))
                .role(Role.USER)
                .isEnabled(true)
                .build();

        if (userRepository.findByUsername(user.getUsername()).isPresent()) throw new UserAlreadyExistsException();
        return userRepository.save(user);
    }

    public AuthenticationToken authenticate(UserCredentials userCredentials) {
        logger.debug("Authenticating user...");

        authenticationmanager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        userCredentials.getUsername(),
                        userCredentials.getPassword()
                )
        );

        User user = userRepository.findByUsername(userCredentials.getUsername())
                .orElseThrow(() -> new UserNotFoundException());

        String token = jwtService.generateToken(user);
        String refreshToken = jwtService.generateRefreshToken(user);

        logger.debug("Tokens generated.");

        return new AuthenticationToken(token, refreshToken);
    }

    public boolean isValid(String token, String username) {
        return jwtService.isValid(token, User.builder().username(username).build());
    }

    public AuthenticationToken refresh(Authentication auth) {

        logger.info("Refreshing with token: {}", auth.getCredentials());

        String refreshToken = (String) auth.getCredentials();
        String username = jwtService.getSubject(refreshToken);

        if (username == null) {
            throw new InvalidTokenException("Token subject not found.");
        }

        User user = userRepository.findByUsername(username)
                .orElseThrow();

        if (!jwtService.isValid(refreshToken, user)) {
            throw new InvalidTokenException();
        }

        String newToken = jwtService.generateToken(user);
        // TODO: set all other tokens to invalid
        // TODO: save token to db

        return AuthenticationToken.builder()
                .token(newToken)
                .refreshToken(refreshToken)
                .build();
    }

    public void logout() {
        SecurityContextHolder.clearContext();

    }


}
