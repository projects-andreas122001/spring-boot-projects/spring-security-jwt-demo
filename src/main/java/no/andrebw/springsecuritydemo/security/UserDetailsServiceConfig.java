package no.andrebw.springsecuritydemo.security;

import lombok.RequiredArgsConstructor;
import no.andrebw.springsecuritydemo.exceptions.UserNotFoundException;
import no.andrebw.springsecuritydemo.model.Role;
import no.andrebw.springsecuritydemo.model.User;
import no.andrebw.springsecuritydemo.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.Optional;

@Configuration
@RequiredArgsConstructor
public class UserDetailsServiceConfig {

    private static final Logger logger = LogManager.getLogger(UserDetailsServiceConfig.class);

    @Value("${admin.username}")
    private String adminUsername;

    @Value("${admin.password}")
    private String adminPassword;

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Bean
    public UserDetailsService userDetailsService() {
        return username -> userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException());
    }

    public UserDetailsService inMemoryUserDetails() {

        User adminDetails = User.builder()
                .username(adminUsername)
                .password(passwordEncoder.encode(adminPassword))
                .role(Role.ADMIN)
                .build();

        User godDetails = User.builder()
                .username("god")
                .password(passwordEncoder.encode("123"))
                .role(Role.OMNIPOTENT)
                .build();

        return new InMemoryUserDetailsManager(adminDetails, godDetails);
    }

}
