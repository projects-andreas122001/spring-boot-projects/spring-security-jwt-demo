package no.andrebw.springsecuritydemo.security;

import io.jsonwebtoken.MalformedJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import no.andrebw.springsecuritydemo.controller.AuthenticationController;
import no.andrebw.springsecuritydemo.exceptions.InvalidTokenException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final Logger logger = LogManager.getLogger(JwtAuthenticationFilter.class);

    private final JwtService jwtService;
    private final UserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        logger.debug("Authenticating request...");

        // Assert existence of authorization-header in request
        final String authHeader = request.getHeader(JwtService.AUTHORIZATION_HEADER);
        if (authHeader == null || !authHeader.startsWith(JwtService.TOKEN_PREFIX)) {
            logger.debug("No authentication-header provided.");

            filterChain.doFilter(request, response);
            return;
        }

        // Extract token from header, and subject from token
        String token = authHeader.substring(JwtService.TOKEN_PREFIX.length());
        String username;
        try {
            username = jwtService.getSubject(token);
        } catch (MalformedJwtException e) {
            filterChain.doFilter(request, response);
            return;
        }

        // Check username and check if we already have an authentication context
        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            if (jwtService.isValid(token, userDetails)) {

                // Set authentication details and context
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                        userDetails,
                        token,
                        userDetails.getAuthorities()
                );
                authToken.setDetails(
                        new WebAuthenticationDetailsSource().buildDetails(request)
                );
                SecurityContextHolder.getContext().setAuthentication(authToken);

                logger.info("Authentication successful: {}", userDetails);
            }
        }

        filterChain.doFilter(request, response);
    }
}
