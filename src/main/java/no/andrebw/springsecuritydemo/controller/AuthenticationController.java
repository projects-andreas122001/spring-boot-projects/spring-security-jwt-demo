package no.andrebw.springsecuritydemo.controller;

import lombok.RequiredArgsConstructor;
import no.andrebw.springsecuritydemo.model.AuthenticationToken;
import no.andrebw.springsecuritydemo.model.User;
import no.andrebw.springsecuritydemo.model.UserCredentials;
import no.andrebw.springsecuritydemo.model.UserDTO;
import no.andrebw.springsecuritydemo.service.AuthenticationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@PreAuthorize("isAuthenticated()")
public class AuthenticationController {

    private static final Logger logger = LogManager.getLogger(AuthenticationController.class);

    private final AuthenticationService authService;

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("permitAll()")
    public User register(@RequestBody UserDTO user) {
        logger.info("Registration request: {}", user);
        return authService.register(user);
    }

    @PostMapping("/authenticate")
    @PreAuthorize("permitAll()")
    @ResponseStatus(HttpStatus.OK)
    public AuthenticationToken authenticate(@RequestBody UserCredentials credentials) {
        logger.info("Authentication request: {}", credentials);

        return authService.authenticate(credentials);
    }

    @PostMapping("/refresh")
    @PreAuthorize("isAuthenticated()")
    @ResponseStatus(HttpStatus.OK)
    public AuthenticationToken refresh(Authentication auth) throws Exception {
        return authService.refresh(auth);
    }

    @PostMapping("/validate")
    @PreAuthorize("permitAll()")
    @ResponseStatus(HttpStatus.OK)
    public boolean isValid(@RequestBody String token, @RequestBody String username) {
        return authService.isValid(token, username);
    }

    @PostMapping("/logout")
    @PreAuthorize("isAuthenticated()")
    @ResponseStatus(HttpStatus.OK)
    public void logout() {
        authService.logout();
    }

}
