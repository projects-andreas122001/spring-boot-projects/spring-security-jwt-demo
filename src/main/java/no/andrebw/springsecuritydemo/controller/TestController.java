package no.andrebw.springsecuritydemo.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
@PreAuthorize("isAuthenticated()")
public class TestController {

    private static final Logger logger = LogManager.getLogger(AuthenticationController.class);


    @GetMapping("/admin")
    @PreAuthorize("hasRole('ADMIN')")
    public String adminTest() {
        return "Ah, I see, you must be an admin.";
    }

    @GetMapping("/public")
    @PreAuthorize("permitAll()")
    public String publicTest() {
        return "I don't know if you are an admin or not.";
    }

    @GetMapping("/name")
    @PreAuthorize("authentication.getPrincipal().getUsername().equals('Obi-Wan')")
    public String nameTest(Authentication authentication) {
        logger.info(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        return "You must be Obi-Wan";
    }
}
