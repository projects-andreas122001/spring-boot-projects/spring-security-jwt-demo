package no.andrebw.springsecuritydemo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "No match found for username and password")
public class InvalidCredentialsException extends RuntimeException {
}
